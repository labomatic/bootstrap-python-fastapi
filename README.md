# bootstrap-python-fastapi

## Description

Inspired by [Noah
Gift](https://github.com/noahgift/python-for-devops-april-2022), this repo is
a bootstrap env for buildnig fastapi microservices using python FastAPI and fire
for cli.

The build process uses [podman](https://podman.io) as container's tool. 

## Quick start

Clone the repo then run:

     make venv
     make all
     make run &
     xdg-open http://0.0.0.0:8080

Now add your code and enjoy !

## Todo

- [ ] add .env file for deploy function and to choose the container tool
    (Docker, crio, ...)
