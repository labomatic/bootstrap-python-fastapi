OBJS:=*.py tests/*.py

venv:
	[ -d venv ] || /usr/bin/python3 -m venv venv

install:
	#install commands
	[ -d venv ] && . venv/bin/activate
	pip install --upgrade pip &&\
		pip install -r requirements.txt

format: ${OBJS}
	#format code
	[ -d venv ] && . venv/bin/activate
	black ${OBJS}
lint: ${OBJS}
	#flake8 or #pylint
	[ -d venv ] && . venv/bin/activate
	which pylint
	pylint --disable=R,C ${OBJS}
test: ${OBJS}
	#test
	[ -d venv ] && . venv/bin/activate
	python3 -m pytest -vv --cov=mylib --cov=main tests/test_*.py
build:
	#build container
	podman build -t deploy-fastapi .
run:
	#run docker
	podman run --rm -p 127.0.0.1:8080:8080 deploy-fastapi
deploy:
	#deploy

all: install format lint test deploy
