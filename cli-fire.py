#!/usr/bin/env python

import fire
from main import app

if __name__ == "__main__":
    fire.Fire(app)
